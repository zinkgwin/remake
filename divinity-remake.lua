-- test
-- made by Zigwin#0000, Escsequence#0000
-- Version 1.7.3.1
-- (overall_updates.new_addition.bug_fixes.small_corrections)
-- 12.01.19
 
-- Options --
 
-- true = yes
-- false = no
-- "Zigwin#0000" - nickname

startGameCountdown = 10
gameStartedTime = os.time()
mapTime = 160
mapTimeDiff8 = 180
 
-- First shaman nickname (if empty then random player)
firstShaman = ""
 
-- Add score with player death
changeScore = true
 
-- Default
defaultDiff = {1,1}
 
-- Options end --

function restoreMapData(diff)
-- Maps list --
local mapsList = {
	{ -- Difficulty 1
        {"7557175",false,false},
        {"7557177",false,false},
        {"7557182",true,false},
        {"7557300",false,false},
        {"7557729",false,false},
        {"7557737",false,false},
        {"6999905",false,false},
        {"6913717",false,false},
        {"7539966",false,false},
        {"7559097",false,false},
	},
	{ -- Difficulty 2
        {"6905859",false,false},
        {"6907826",false,false},
        {"7557180",false,false},
        {"6942506",false,false},
        {"7159157",false,false},
        {"7309096",false,false},
        {"7557283",false,false},
        {"7557741",false,false},
        {"7557301",false,false},
        {"7557446",false,false},
        {"7557732",false,false},
	},
	{ -- Difficulty 3
        {"6907218",false,false},
        {"7070133",false,false},
        {"7158737",false,false},
        {"7111566",false,false},
        {"7109802",false,false},
        {"7317168",false,false},
        {"7460600",false,false},
        {"6887601",false,false},
        {"7557362",true,false},
        {"7543988",true,false},
        {"7383302",false,false},
        {"7557448",false,false},
        {"7557745",false,false},
        {"7557798",false,false},
        {"1670714",false,false},
	},
	{ -- Difficulty 4
        {"6887608",false,false},
        {"6911915",false,false},
        {"7157145",false,false},
        {"7286441",false,false},
        {"7110415",true,true},
        {"6883778",false,false},
        {"6889940",false,false},
        {"6895891",false,false},
        {"7511186",false,false},
        {"6890001",false,false},
        {"7362219",false,true},
        {"7557365",false,true},
        {"6913732",false,false},
        {"6907241",true,false},
        {"7375577",true,false},
        {"7558816",false,false},
        {"7558756",false,false},
        {"7558764",false,false},
        {"7559039",false,false}
	},
	{ -- Difficulty 5
        {"6909060",false,false},
        {"6976351",false,false},
        {"7073765",true,false},
        {"7432986",false,false},
        {"7317174",true,false},
        {"7114382",false,false},
        {"7558144",false,false},
        {"7551450",true,false},
        {"6889917",true,false},
        {"7557308",true,false},
        {"7275044",false,false},
        {"7371944",false,false},
        {"7275020",false,false},
        {"6915494",false,false},
        {"7320003",false,false},
        {"7465969",false,false},
        {"7447449",false,false},
        {"6937693",false,false},
        {"7336047",false,false},
        {"7368707",false,false},
        {"7269943",true,false},
        {"7557770",true,false},
	},
	{ -- Difficulty 6
        {"6935536",true,false},
        {"6909541",true,false},
        {"7554443",true,false},
        {"7557142",true,false},
        {"6994640",true,false},
        {"7163242",false,false},
        {"7396085",false,false},
        {"7073746",false,false},
        {"7361560",true,false},
        {"7359266",true,false},
        {"7359266",true,false},
        {"7280375",false,false},
        {"7479194",false,false},
        {"7394894",false,false},
        {"7112495",false,false},
        {"7348658",true,false},
        {"7503463",false,false},
	},
	{ -- Difficulty 7
        {"7272683",true,false},
        {"6994741",true,false},
        {"6928961",true,false},
        {"7345752",true,false},
        {"7553498",true,false},
        {"7476836",true,false},
        {"7558118",true,true},
        {"7557753",true,false},
        {"7370212",true,false},
        {"7340965",false,false},
        {"7292681",false,false},
        {"7127926",false,false},
        {"7553701",false,false},
        {"7365669",false,false},
        {"7365776",true,false},
        {"7553423",false,false},
        {"7557836",true,false},
	},
	{ -- Difficulty 8
        {"7487563",false,false},
        {"7288343",false,false},
        {"7270197",true,false},
        {"7148113",false,false},
        {"7398377",true,false},
	},
}

	if diff then
		return mapsList[diff]
	else
		return mapsList
	end
end

-- Main Code --
 
local playerData = {}
local ballonsSpawned, cannonSpawned = 0, 0
local isBallonsAllowed, isCannonAllowed = true, true
local map = ""
local nextShaman = ""
local diff = 0
local isStatAdded = false
local availableMaps = restoreMapData()

--[[ playerData init ]]--

function eventNewPlayer(playerName)
	playerData[playerName]={
		diff = defaultDiff,
		isAfk = false,
		stats = {
			diff = {
				{played = 0, completed = 0},
				{played = 0, completed = 0},
				{played = 0, completed = 0},
				{played = 0, completed = 0},
				{played = 0, completed = 0},
				{played = 0, completed = 0},
				{played = 0, completed = 0},
				{played = 0, completed = 0},
			}
		},
	}
	tfm.exec.bindKeyboard(playerName,80,true,true)
	tfm.exec.setPlayerScore(playerName, 0)
	ui.addPopup(0, 0, "Commands\n!diff 1-8\t- Difficulty\n!afk\t\t- Skip shaman mode\n!status\t- Check data\n!skip\t\t- End turn\n!p or P\t- Check your profile\n!p name\t- Check other player's profile\n!info\t\t- Gravitation & Wind", playerName, 280, 100, 260, true)
end
table.foreach(tfm.get.room.playerList, eventNewPlayer)
 
--[[ First shaman init ]]--

if firstShaman ~= "" then
	tfm.exec.setPlayerScore(firstShaman, 1)
else
	local playerList = {}
	for name in next, playerData do
		table.insert(playerList, name)
	end
 
	tfm.exec.setPlayerScore(playerList[math.random(1,#playerList)], 1)
end
firstShaman = nil

function eventPlayerLeft(playerName)
   playerData[playerName].isAfk = true
end

--[[ Map and Shaman choosing before map changes ]]--

function nextMap()
	math.randomseed(os.time())
	isStatAdded = false
	ballonsSpawned = 0
	cannonSpawend = 0
    
    local max = -1
	for name, data in next, playerData do
		if not data.isAfk then
			local score = tfm.get.room.playerList[name].score
		   
			if score > max then
				max = score
				nextShaman = name      
			end
		end
	end
	local equalScore = {}
	for name, data in next, playerData do
		if not data.isAfk then
			local score = tfm.get.room.playerList[name].score
 
			if score == max then
				table.insert(equalScore, name)
			end
		end
	end
	if #equalScore > 1 then
		nextShaman = equalScore[math.random(1,#equalScore)]
	end

	diff = math.random(playerData[nextShaman].diff[1],playerData[nextShaman].diff[2])

	local previousMap = map
	local randomMapIndex = ""

	while map == previousMap do
		randomMapIndex = math.random(1,#availableMaps[diff])
		map = availableMaps[diff][randomMapIndex][1] 
	end

	isBallonsAllowed = not availableMaps[diff][randomMapIndex][2]
	isCannonAllowed = not availableMaps[diff][randomMapIndex][3]
 
	tfm.exec.newGame(map)

	-- Removing Map from list
	if #availableMaps[diff] > 1 then
		table.remove(availableMaps[diff], randomMapIndex)
	elseif #availableMaps[diff] == 1 then
		availableMaps[diff] = restoreMapData(diff)
	end
end

function showProfile(playerNameProfile, playerName, show)
	if show then
		playerData[playerName].isProfileOpen = true
		local isNickname = false
		for name in next, playerData do
			if playerNameProfile == name then
				isNickname = true
				break
			end
		end
		if not isNickname then
			return
		end
	
		local name = playerNameProfile:sub(1,#playerNameProfile-5)
		local tag = playerNameProfile:sub(#playerNameProfile-4,#playerNameProfile)
	
		playerData[playerName].isProfileOpen = true
	
		ui.addTextArea(0, "", playerName, 300, 50, 200, 210, 0x8f563b, 0x8f563b, 1, true)
		ui.addTextArea(1, "", playerName, 480, 240, 20, 20, 0x847e87, 0x847e87, 1, true)
		ui.addTextArea(2, "", playerName, 300, 240, 20, 20, 0x847e87, 0x847e87, 1, true)
		ui.addTextArea(3, "", playerName, 480, 50, 20, 20, 0x847e87, 0x847e87, 1, true)
		ui.addTextArea(4, "", playerName, 300, 50, 20, 20, 0x847e87, 0x847e87, 1, true)
		ui.addTextArea(5, "", playerName, 305, 55, 190, 200, 0x663931, 0x8f563b, 1, true)
	
		local profileText="<p align='center'><font face='Arial Black' size='18'><v>"..name.."</v><g>"..tag.."</g></font></p>\n<n> Difficulty</n>\n<ch>\t   Played / Completed / Ratio\n"
		local shadowText="<font color='#241125'><p align='center'><font face='Arial Black' size='18'>"..playerNameProfile.."</font></p>\n Difficulty\n\t   Played / Completed / Ratio\n"
	
		for i = 1, 8 do
			local ratio = tostring(playerData[playerNameProfile].stats.diff[i].completed / playerData[playerNameProfile].stats.diff[i].played):sub(1,5)
	
			if playerData[playerNameProfile].stats.diff[i].played == 0 then
				ratio = 0
			end
	
			shadowText = shadowText.."\t● "..i.." : "..playerData[playerNameProfile].stats.diff[i].played.." / "..playerData[playerNameProfile].stats.diff[i].completed.." / "..ratio.."\n"
			
			local numberRatio = tonumber(ratio)
	
			if playerData[playerNameProfile].stats.diff[i].played == 0 then
				ratio = "<n>0</n>"
			elseif numberRatio >= 0.8 then
				ratio = "<vp>"..ratio.."</vp>"
			elseif numberRatio >= 0.5 then
				ratio = "<j>"..ratio.."</j>"
			elseif numberRatio >= 0.3 then
				ratio = "<font color='#E56019'>"..ratio.."</font>"
			else
				ratio = "<r>"..ratio.."</r>"
			end
	
			profileText = profileText.."\t● "..i.." : <n>"..playerData[playerNameProfile].stats.diff[i].played.."</n> <g>/</g> <n>"..playerData[playerNameProfile].stats.diff[i].completed.."</n> <g>/</g> "..ratio.."\n"
		end
	
		--ui.addTextArea(6, shadowText.."\n\tTotal : 0 / 0 / 0", playerNameProfile, 301, 51, 200, 200, nil, nil, 0, true)
		--ui.addTextArea(7, profileText.."\n\tTotal : <n>0</n> / <n>0</n> / 0", playerNameProfile, 300, 50, 200, 200, nil, nil, 0, true)
	
		ui.addTextArea(7, shadowText, playerName, 301, 51, 200, 200, nil, nil, 0, true)
		ui.addTextArea(8, profileText, playerName, 300, 50, 200, 200, nil, nil, 0, true)
	
		ui.addTextArea(6, "<p align='center'><a href='event:closeProfile'>Close", playerName, 300, 227, 200, 30, 0x324650, 0x000000, 0, true)
	else
		playerData[playerName].isProfileOpen = false
		for i = 0, 8 do
			ui.removeTextArea(i, playerName)
		end
	end
end
 
function eventPlayerDied(playerName)
	if changeScore then
		if not tfm.get.room.playerList[playerName].isShaman then
			tfm.exec.setPlayerScore(playerName, 1, true)		
		else
			-- Shaman dies - Set time to 20
			tfm.exec.setGameTime(20, true)
			-- Shaman dies - Add 1 score to all alive players
			for name, data in next, tfm.get.room.playerList do
				if (not (data.isDead or data.isWon)) or (not data.isShaman) then
					tfm.exec.setPlayerScore(name, 1, true)
				end
    		end	
		end
	end
 
	for name, data in next, tfm.get.room.playerList do
		if not (data.isDead or data.isWon) then
			return
		end
    end
    if gameStartedTime >= os.time()-3000 then
        tfm.exec.setGameTime(3, true)
    else
        nextMap()
    end
end
 
function eventPlayerWon(playerName)
	if not isStatAdded then
		playerData[nextShaman].stats.diff[diff].completed = playerData[nextShaman].stats.diff[diff].completed + 1
		isStatAdded = true
	end

	if not tfm.get.room.playerList[playerName].isShaman then
		tfm.exec.setPlayerScore(playerName, 10, true)
	end

	for _, data in next, tfm.get.room.playerList do
		if not (data.isDead or data.isWon) then
			return
		end
	end
	nextMap()
end

function eventNewGame()
    gameStartedTime = os.time()
    ballonsSpawned = 0
    
	tfm.exec.setGameTime(diff ~= 8 and mapTime or mapTimeDiff8)

	local author = "<j>"..tfm.get.room.xmlMapInfo.author.."</j>"
	local mapCode = "@"..tfm.get.room.xmlMapInfo.mapCode
	local miscText = ""
   
	if not isCannonAllowed then 
		if not isBallonsAllowed then
			miscText = "   <g>|</g>  <r> No ballon & cannon map</r>"
		else
			miscText = "   <g>|</g>  <r> No cannon map</r>"
		end
	elseif not isBallonsAllowed then
		miscText = "   <g>|</g>  <r> No balloon map</r>"
	end

	local colorList = {"#5b6ee1","#5fcde4","#49c491","#8bd149","#ded531","#d45e0f","#b31b1b","#9d00db",}
	local diffColor = colorList[diff]

	tfm.exec.setShaman(nextShaman)
	tfm.exec.setShamanMode(nextShaman, 2)
	tfm.exec.setUIShamanName(nextShaman)
	--tfm.exec.setNameColor(nextShaman, 0xff7777)
 
	tfm.exec.setPlayerScore(nextShaman, 0)

	playerData[nextShaman].stats.diff[diff].played = playerData[nextShaman].stats.diff[diff].played + 1
 
	tfm.exec.setUIMapName(author.." <bl>- "..mapCode.."</bl> : <font color='"..diffColor.."'>D"..diff.."</font>"..miscText)
end
 
function eventSummoningEnd(playerName, objectType, xPosition, yPosition, angle, xSpeed, ySpeed, other) 
	-- On Ballon Spawn
	if tostring(objectType):sub(1,2) == "28" then
		if isBallonsAllowed then
			if not other.ghost then
				ballonsSpawned = ballonsSpawned + 1
	   
				if ballonsSpawned > 3 then
					tfm.exec.removeObject(other.id)
				end
			end
		else
			tfm.exec.removeObject(other.id)
		end
	end

	-- On Cannon Spawn
	if tostring(objectType):sub(1,2) == "17" then
		if not isCannonAllowed then
			tfm.exec.killPlayer(playerName)
			tfm.exec.addShamanObject(0,200,50,180,50,-20,false)
		end
	end
end

function eventTextAreaCallback(textAreaId, playerName, callback)
	if callback == "closeInfo" then
		for id=100,105 do
			ui.removeTextArea(id, playerName)
		end
	end
	if callback == "closeProfile" then
		showProfile(nil,playerName,false)
	end
end

function eventChatCommand(playerName, message)
	local args = {}
	for s in message:gmatch("[%S]+") do
		table.insert(args, s)
	end

	if args[1] == "p" then
		if playerData[playerName].isProfileOpen then
			showProfile(nil, playerName, false)
		else
			if not args[2] then
				showProfile(playerName, playerName, true)
			else
				showProfile(args[2], playerName, true)
			end
		end
	end
	if args[1] == "map" then
		nextMap(args[2]:gsub("@", ""))
	end
	if message == "info" then
		local xml = tfm.get.room.xmlMapInfo.xml		

		local info = {}
		for wind, gravitation in xml:gmatch("G=\"(%-?%d+),(%-?%d+)\"") do
			info = {wind, gravitation}
		end

		if #info == 0 then
			info = {0,10}
		end

		ui.addTextArea(100, "", playerName, 300, 50, 200, 50, 0x8f563b, 0x8f563b, 1, true)
		ui.addTextArea(101, "", playerName, 480, 80, 20, 20, 0x847e87, 0x847e87, 1, true)
		ui.addTextArea(102, "", playerName, 300, 50, 20, 20, 0x847e87, 0x847e87, 1, true)
		ui.addTextArea(103, "Wind : "..(info[1]).."\nGravitation : "..(info[2]), playerName, 305, 55, 190, 40, 0x663931, 0x8f563b, 1, true)
		ui.addTextArea(104, "", playerName, 300, 90, 200, 10, 0x663931, 0x8f563b, 1, true)
		ui.addTextArea(105, "<p align='center'><a href='event:closeInfo'>Close", playerName, 300, 87, 200, 30, 0x324650, 0x000000, 0, true)
	end
	if message == "skip" then
		if tfm.get.room.playerList[playerName].isShaman then
			nextMap()
		end
	end
	if message:sub(1,4) == 'diff' then
		if message:sub(7,7) ~= '-' then
			return
		end
 
		local diff = {(tonumber(message:sub(6,6)) or 1),(tonumber(message:sub(8,8)) or 1)}
 
		if diff[2] < diff[1] then
			diff = {diff[2], diff[1]}
		end
 
		if diff[1] > 0 and diff[1] <= 8 and diff[2] > 0 and diff[2] <= 8 then
			playerData[playerName].diff = diff
		end
	end
	if message == "afk" then
		if playerData[playerName].isAfk then
			playerData[playerName].isAfk = false
			tfm.exec.setNameColor(playerName, 0)
		else
			playerData[playerName].isAfk = true
			tfm.exec.setNameColor(playerName, 0xdd0000)
			tfm.exec.killPlayer(playerName)
		end
	end
	if message == "status" then
		local afk = "Yes"
		if not playerData[playerName].isAfk then
			afk = "No"
		end
		ui.addPopup(0, 0, "Afk : "..afk.."\nDifficulty : "..tostring(playerData[playerName].diff[1]).." - "..tostring(playerData[playerName].diff[2]).."", playerName, 300, 100, 200, true)
	end
	if message == "m" and tfm.get.room.playerList[playerName].isShaman then
		tfm.exec.killPlayer(playerName)
	end
	if message == "k" and tfm.get.room.playerList[playerName].isShaman then		
		local isAnyAlive = function()
			local alive = false
			for playerName, playerData in next, tfm.get.room.playerList do
				if not playerData.isDead and not playerData.isShaman then
					alive = true
				end
			end
			return alive
		end

		if isAnyAlive() then
			for playerName, playerData in next, tfm.get.room.playerList do
				if not playerData.isShaman then
					if timeRemaining < 20 then
						tfm.exec.killPlayer(playerName)
						tfm.exec.setGameTime(20, true)
					end
				end
			end
		end
	end
end

function eventKeyboard(playerName, keyCode)
	if keyCode == 80 then
		eventChatCommand(playerName, "p")
	end
end
 
function eventLoop(currentTime, remaining)
	timeRemaining = remaining / 1000
	if remaining <= 0 then
		nextMap()
	end
end

-- Chat Commands
system.disableChatCommandDisplay("info", true)
system.disableChatCommandDisplay("status", true)
system.disableChatCommandDisplay("m", true)
system.disableChatCommandDisplay("p", true)

-- Setup game
tfm.exec.disableAutoNewGame(true)
tfm.exec.disableAutoScore(true)
tfm.exec.disableAutoShaman(true)
tfm.exec.disableAllShamanSkills(true)
math.randomseed(os.time())

-- start
tfm.exec.setGameTime(startGameCountdown)